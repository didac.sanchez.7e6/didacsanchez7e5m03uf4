﻿using System;
using System.Collections.Generic;

namespace PooP1
{
    class Program
    {
        /*--- Interficies ---*/
        public interface IOrdenable
        {
            int Comparar(IOrdenable x);
        };

        /*--- Classes ---*/

        abstract class FiguraGometrica : IOrdenable
        {

            /*--- Propiedades ---*/

            protected int Codi { get; set; }
            protected string Nom { get; set; }
            protected ConsoleColor Color { get; set; }

            /*--- Constructores ---*/

            public FiguraGometrica() 
            {
                Codi = 0;
                Nom = "";
                Color = ConsoleColor.Black;
            }

            public FiguraGometrica(int codi, string nom, ConsoleColor color)
            {
                this.Codi = codi;
                this.Nom = nom;
                this.Color = color;
            }
            public FiguraGometrica(FiguraGometrica figura)
            {
                Codi = figura.Codi;
                Nom = figura.Nom;
                Color = figura.Color;
            }

            /*--- Get & Set ---*/

            public void SetCodi(int codi)
            {
                this.Codi = codi;
            }

            public void SetNom(string nom)
            {
                this.Nom = nom;
            }

            public void SetColor(ConsoleColor color)
            {
                this.Color = color;
            }

            public int GetCodi()
            {
                return this.Codi;
            }

            public string GetNom()
            {
                return this.Nom;
            }

            public ConsoleColor GetColor()
            {
                return this.Color;
            }

            /*--- Metodos Hererados ---*/
            public int Comparar(IOrdenable x)
            {
                FiguraGometrica fig = (FiguraGometrica)x;
                if (this.Area() == fig.Area()) return 0;
                if (this.Area() >= fig.Area()) return -1;
                return 1;
            }

            /*--- Metodos ---*/

            public override bool Equals(object obj)
            {
                FiguraGometrica figura = (FiguraGometrica)obj;
                return this.Codi == figura.Codi;
            }

            public override int GetHashCode()
            {
                return Convert.ToInt32(Math.Truncate(this.Codi * 2 - this.Codi / Math.PI));
            }

            public override string ToString()
            {
                return $"Codi : {this.Codi}, Nom : {this.Nom}, Color : {this.Color}";
            }

            abstract public double Perimetre();

            abstract public double Area();

        }

        class Retangle : FiguraGometrica
        {

            /*--- Atrivuts ---*/

            private double DistanciaBase { get; set; }
            private double Altura { get; set; }

            /*--- Constructores ---*/

            public Retangle() : base()
            {
                this.DistanciaBase = 0;
                this.Altura = 0;
            }
            public Retangle(double distanciaBase, double altura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.DistanciaBase = distanciaBase;
                this.Altura = altura;
            }
            public Retangle(Retangle figura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                DistanciaBase = figura.DistanciaBase;
                Altura = figura.Altura;
            }

            /*--- Get & Set ---*/

            public double GetDistanciaBase()
            {
                return DistanciaBase;
            }
            public void SetDistanciaBase(double DistanciaBase)
            {
                this.DistanciaBase = DistanciaBase;
            }
            public double GetAltura()
            {
                return Altura;
            }
            public void SetAltura(double Altura)
            {
                this.Altura = Altura;
            }

            /*--- Metodos ---*/

            public override string ToString()
            {
                return $"Base : {DistanciaBase}, Altur : {Altura} " + base.ToString();
            }
            public override double Area()
            {
                return this.DistanciaBase * this.Altura;
            }
            public override double Perimetre()
            {
                return this.DistanciaBase * 2 + this.Altura * 2;
            }

        }

        class Triangle : FiguraGometrica
        {

            /*--- Atrivuts ---*/

            private double DistanciaBase { get; set; }
            private double Altura { get; set; }

            /*--- Constructores ---*/

            public Triangle() : base()
            {
                this.DistanciaBase = 0;
                this.Altura = 0;
            }
            public Triangle(double distanciaBase, double altura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.DistanciaBase = distanciaBase;
                this.Altura = altura;
            }
            public Triangle(Triangle figura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                DistanciaBase = figura.DistanciaBase;
                Altura = figura.Altura;
            }

            /*--- Get & Set ---*/

            public double GetDistanciaBase()
            {
                return DistanciaBase;
            }
            public void SetDistanciaBase(double DistanciaBase)
            {
                this.DistanciaBase = DistanciaBase;
            }
            public double GetAltura()
            {
                return Altura;
            }
            public void SetAltura(double Altura)
            {
                this.Altura = Altura;
            }

            /*--- Metodos ---*/

            public override string ToString()
            {
                return $"Base : {DistanciaBase}, Altur : {Altura} " + base.ToString();
            }
            public override double Area()
            {
                return this.DistanciaBase * this.Altura;
            }
            public override double Perimetre()
            {
                return this.DistanciaBase * 2 + this.Altura * 2;
            }
        }

        class Cercle : FiguraGometrica
        {

            /*--- Parametres ---*/

            private double Radi;

            /*--- Constructores ---*/

            public Cercle() : base()
            {
                this.Radi = 0;

            }
            public Cercle(double radi, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.Radi = radi;

            }
            public Cercle(Cercle figura) : base(figura)
            {
                this.Radi = figura.Radi;
            }

            /*--- Metodos ---*/

            public double GetRadi()
            {
                return Radi;
            }
            public void SetRadi(double radi)
            {
                this.Radi = radi;
            }
            public override string ToString()
            {
                return $"Radi : {Radi} " + base.ToString();
            }
            public override double Perimetre()
            {
                return (Radi * 2 * Math.PI);
            }
            public override double Area()
            {
                return (Math.Pow(Radi, 2) * Math.PI);
            }

        }

        public static List<IOrdenable> Ordenar(List<IOrdenable> obj)
        {
            obj.Sort((x, y) => x.Comparar(y));
            return obj;
        }

        class ProvaFigures
        {
            static void Main()
            {
                // Creacio cercles
                Cercle cercle1 = new Cercle();
                Cercle cercle2 = new Cercle(2.5, 1,  "Circulo", ConsoleColor.Red);
                Cercle cercle3 = new Cercle(cercle2);
                // Get Set
                Console.WriteLine(cercle1.GetRadi());
                cercle1.SetRadi(2);
                Console.WriteLine(cercle1.GetRadi());
                // Area
                Console.WriteLine(cercle3.Area());
                Console.WriteLine(cercle2.Area());
                Console.WriteLine(cercle1.Area());
                // Perimetre
                Console.WriteLine(cercle3.Perimetre());
                Console.WriteLine(cercle2.Perimetre());
                Console.WriteLine(cercle1.Perimetre());
                // Info
                Console.WriteLine(cercle3.ToString());
                Console.WriteLine(cercle2.ToString());
                Console.WriteLine(cercle1.ToString());
                // GetHashCode
                Console.WriteLine(cercle3.GetHashCode());
                Console.WriteLine(cercle2.GetHashCode());
                Console.WriteLine(cercle1.GetHashCode());
                // Equals
                Console.WriteLine(cercle3.Equals(cercle2));
                Console.WriteLine(cercle3.Equals(cercle1));
                // Ordenar
                Triangle trianglre2 = new Triangle(2.5, 5.0, 8, "Triangulo", ConsoleColor.Blue);
                Retangle retangle2 = new Retangle(2.5, 5,12, "Rectangle", ConsoleColor.Green);
                // See
                Console.WriteLine(trianglre2.Area());
                Console.WriteLine(retangle2.Area());
                List<IOrdenable> array = new List<IOrdenable> { retangle2, cercle1, cercle2, cercle3, trianglre2 };
                Ordenar(array);
                array.ForEach(data => Console.WriteLine(data));

                /* Triangle trianglre1 = new Triangle();
                 Triangle trianglre2 = new Triangle(2.5, 5);
                 Triangle trianglre3 = new Triangle(trianglre2);
                 Console.WriteLine($"{trianglre1.GetAltura()}, {trianglre1.GetDistanciaBase()}");
                 trianglre1.SetAltura(2);
                 trianglre1.SetDistanciaBase(4);
                 Console.WriteLine($"{trianglre1.GetAltura()}, {trianglre1.GetDistanciaBase()}");
                 Console.WriteLine(trianglre3.Area());
                 Console.WriteLine(trianglre2.Area());
                 Console.WriteLine(trianglre1.Area());
                 Console.WriteLine(trianglre3.Perimetre());
                 Console.WriteLine(trianglre2.Perimetre());
                 Console.WriteLine(trianglre1.Perimetre());
                 Console.WriteLine(trianglre3.ToString());
                 Console.WriteLine(trianglre2.ToString());
                 Console.WriteLine(trianglre1.ToString());


                 Retangle retangle1 = new Retangle();
                 Retangle retangle2 = new Retangle(2.5, 5);
                 Retangle retangle3 = new Retangle(retangle2);
                 Console.WriteLine($"{retangle1.GetAltura()}, {retangle1.GetDistanciaBase()}");
                 retangle1.SetAltura(2);
                 retangle1.SetDistanciaBase(4);
                 Console.WriteLine($"{retangle1.GetAltura()}, {retangle1.GetDistanciaBase()}");
                 Console.WriteLine(retangle3.Area());
                 Console.WriteLine(retangle2.Area());
                 Console.WriteLine(retangle1.Area());
                 Console.WriteLine(retangle3.Perimetre());
                 Console.WriteLine(retangle2.Perimetre());
                 Console.WriteLine(retangle1.Perimetre());
                 Console.WriteLine(retangle3.ToString());
                 Console.WriteLine(retangle2.ToString());
                 Console.WriteLine(retangle1.ToString());*/
            }
        }

    }
}
